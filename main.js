import './main.sass'
import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import ClickWell from './views/ClickWell.vue'
import CompareNumbers from './views/CompareNumbers.vue'
import StoppedOrNot from './views/StoppedOrNot.vue'

Vue.use(Router)
new Vue({
  el: '#app',
  render: h => h('router-view'),
  router: new Router({
    routes: [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: '/clickwell',
        name: 'ClickWell',
        component: ClickWell
      },
      {
        path: '/comparenumbers',
        name: 'CompareNumbers',
        component: CompareNumbers
      },
      {
        path: '/stoppedornot',
        name: 'StoppedOrNot',
        component: StoppedOrNot
      }
    ]
  })
})